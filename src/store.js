import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// Call from console:
// document.getElementsByTagName('a')[0].__vue__.$store._mutations.increment[0]()

export default new Vuex.Store({
  state: {
    count: 0
  },
  mutations: {
    increment (state) {
      state.count++
    }
  },
  actions: {

  }
})
